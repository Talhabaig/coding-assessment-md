import React, { useState } from "react";
import axios from "axios";
import ReactTable from "react-table";
import 'react-table/react-table.css'

export const FileUpload = () => {
  const [file, setFile] = useState();
  const [fileName, setFileName] = useState();
  const [selectedFile, setSelectedFile] = useState()
    
  const saveFile = (e) => { 
    setFile(e.target.files[0]);
    setFileName(e.target.files[0].name);
   
  };

  const uploadFile = async (e) => { 
    const formData = new FormData();
    formData.append("formFile", file);
    formData.append("fileName", fileName);
    try {
      const res = await axios.post("https://localhost:44323/api/file", formData);
      debugger
      const data = res.data
      setSelectedFile({data: data})
      console.log(res.data);
    } catch (ex) {
      console.log(ex);
    }
  } 
  return (
    <>
      <h2>Please Upload csv file</h2>
      <input type="file" accept=".csv" onChange={saveFile} />
      <input type="button" value="upload" onClick={uploadFile} /> 
      {selectedFile ? (
        <div>
        <h3 className='text-center'>Total revenue for all products sold :{selectedFile.data.grandTotalRevenue}</h3>
        <h3 className='text-center'>The month with the highest revenue :{selectedFile.data.monthlyRevenue}</h3>
        <h3 className='text-center'>The most popular ProductId based on quantity sold :{selectedFile.data.popularProduct}</h3> 

        <ReactTable  
      data={selectedFile.data.quantitySold}  
      columns={[{  
        Header: 'ProductId',  
        accessor: 'productId',
       }
       ,{  
        Header: 'Quantity',  
        accessor: 'quantity' ,
        }
       
       ,{  
       Header: 'SaleTime',  
       accessor: 'saleTime' ,
       }
       ,{  
       Header: 'UnitPrice',  
       accessor: 'unitPrice',
       },
       {  
        Header: 'month',  
        accessor: 'month',
        },
        {  
          Header: 'TotalRevenue',  
          accessor: 'totalRevenue',
          }]}
            
   />
         
        
        </div>) : null}
      
    </>
  );
};
