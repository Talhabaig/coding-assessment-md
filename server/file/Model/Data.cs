﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace file.Model
{
    public class Data
    {
        public long ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public DateTime SaleTime { get; set; }
    }
    public class FinalResult
    {
        public long PopularProduct { get; set; }
        public long PopularQuantity { get; set; }
        public decimal GrandTotalRevenue { get; set; }
        public string MonthlyRevenue { get; set; }
        public List<QuantitySold> QuantitySold { get; set; }
        public FinalResult()    
        {
            QuantitySold = new List<QuantitySold>();
        }
    }
    public class QuantitySold
    {
        public long ProductId { get; set; } 
        public int Quantity { get; set; } 
        public DateTime SaleTime { get; set; }
        public decimal UnitPrice { get; set; }
        public string month { get; set; }
        public decimal TotalRevenue { get; set; } 
    }
}
