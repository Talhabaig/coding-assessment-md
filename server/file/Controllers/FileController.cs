﻿
using file.Model;
using LumenWorks.Framework.IO.Csv;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace file.Controllers
{
    [Route("api/file")]
    [ApiController]
    public class FileController : ControllerBase
    {

        [HttpPost]
        public FinalResult Post([FromForm] FileModel file)
        {
            List<Data> data = new List<Data>();
            FinalResult results = new FinalResult();
            try
            {
                string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", file.FileName);

                using (Stream stream = new FileStream(path, FileMode.Create))
                {
                    file.FormFile.CopyTo(stream);
                }
                var csvTable = new DataTable();
                using (var csvReader = new CsvReader(new StreamReader(System.IO.File.OpenRead(path)), true))
                {
                    csvTable.Load(csvReader);
                }
                for (int i = 0; i < csvTable.Rows.Count; i++)
                {
                    data.Add(new Data
                    {
                        ProductId = Convert.ToInt64(csvTable.Rows[i][0].ToString()),
                        Quantity = Convert.ToInt32(csvTable.Rows[i][1].ToString()),
                        UnitPrice = Convert.ToDecimal(csvTable.Rows[i][2].ToString()),
                        SaleTime = Convert.ToDateTime(csvTable.Rows[i][3].ToString())
                    });
                }

                //    List<QuantitySold> results = (
                //from p in data
                //group p by new { p.ProductId , p.UnitPrice} into g
                //select new QuantitySold()
                //{
                //    ProductId = g.Key.ProductId,
                //    UnitPrice = g.Key.UnitPrice,
                //    Quantity = g.Select(c => c.Quantity).Sum(),
                //    TotalRevenue = Convert.ToDecimal(g.Select(c => c.Quantity).Sum()) * Convert.ToDecimal(g.Select(c => c.UnitPrice))
                //}
                //).OrderBy(x=>x.ProductId).ToList();

                foreach (var singleProduct in data)
                {
                    if (!results.QuantitySold.Any(x => x.ProductId == singleProduct.ProductId))
                    {
                        var quantity = 0;
                        var unitPrice = singleProduct.UnitPrice;
                        foreach (var compareProduct in data)
                        {
                            if (singleProduct.ProductId == compareProduct.ProductId)
                            {
                                quantity = quantity + compareProduct.Quantity;
                                if (unitPrice != compareProduct.UnitPrice)
                                {
                                    unitPrice = unitPrice + compareProduct.UnitPrice;
                                }
                            }
                        }

                        results.QuantitySold.Add(
                            (new QuantitySold
                            {
                                ProductId = singleProduct.ProductId,
                                Quantity = quantity,
                                TotalRevenue = quantity * unitPrice,
                                UnitPrice = singleProduct.UnitPrice,
                                SaleTime = singleProduct.SaleTime,
                                month = singleProduct.SaleTime.ToString("MMMM")
                            })
                            );
                    }

                }

                var month = results.QuantitySold.GroupBy(b => new { b.month })
                 .Select(g => new { g.Key, tr = g.Sum(s => s.TotalRevenue) }).OrderByDescending(x => x.tr).FirstOrDefault();
                var popularProduct = results.QuantitySold.OrderByDescending(x => x.Quantity).GroupBy(x => x.ProductId).Select(x => x.First()).FirstOrDefault();
                results.PopularProduct = popularProduct.ProductId;
                results.PopularQuantity = popularProduct.Quantity;
                results.GrandTotalRevenue = results.QuantitySold.Sum(x => x.TotalRevenue);
                results.MonthlyRevenue = month.Key.month;

            }
            catch (Exception ex)
            {
                return null;
            }

            return results;
        }

    }
}
